package;

import hxmath.math.Vector2;
import haxe.Timer;
import kha.Game;
import kha.Framebuffer;
import kha.Configuration;
import kha.graphics2.Graphics;
import kha.Image;
import kha.Scaler;
import kha.math.Matrix3;
import kha.input.Surface;
import kha.Loader;

class Khawater extends Game {
	var backbuffer: Image;
	var levelInitiated = false;
	var flow = false;
	var dt = 0.0;
	var previousTimestemp = 0.0;
	public static var solver = new Solver();

	public function new() {
		super("Khawater", false);
	}

	public override function init()
	{
		backbuffer = Image.createRenderTarget(1280, 720);
		Loader.the.loadRoom("res", initLevel);
	}

	function initLevel()
	{
		Configuration.setScreen(this);
		levelInitiated = true;
	}

	public override function render(frame: Framebuffer)
	{
		if (!levelInitiated && (solver==null)) return;
		var g = backbuffer.g2;
		g.begin();
		g.clear();
		for (drop in solver.particles)
		{
			g.drawImage(Loader.the.getImage("drop"),drop.pos.x,drop.pos.y);
		}
		g.end();
		
		startRender(frame);
		Scaler.scale(backbuffer,frame,kha.Sys.screenRotation);
		endRender(frame);
	}

	override function mouseDown(x: Int, y: Int)
	{
		flow = true;
		solver.flowSrc = new Vector2(x,y);
	}

	override function mouseMove(x: Int, y: Int)
	{
		if (flow)
			solver.flowSrc = new Vector2(x,y);
	}

	override function mouseUp(x: Int, y: Int)
	{
		flow = false;
	}

	override function update()
	{
		var currentTime = Timer.stamp();
		dt = Timer.stamp() - previousTimestemp;
		previousTimestemp = currentTime;

		if (levelInitiated) {solver.update(dt);}
	}

}