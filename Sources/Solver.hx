package;

import hxmath.math.Vector2;

class Solver
{
	public var particles = new List<Drop>();
	public var flowSrc = new Vector2(10,10);
	var sHash = new SpartialHash<Drop>(16);
	var gravity = new Vector2(0,900);
	var stiffnessK = 0.504;
	var sigmaViscousity = 0.204;
	var betaViscousity = 0.504;
	var p0 = 10;
	var maxCons = 10;
	var maxConDistance = 10;
	var maxParticles = 500;

	public function new(){
		init();
	}

	public function addParticle(x: Float, y: Float) : Drop
	{
		var d = new Drop(x,y);
		particles.add(d);
		sHash.insert(new Vector2(x,y),d);
		return d;
	}

	public function delParticle(d: Drop)
	{
		particles.remove(d);
	}

	function init()
	{
	}

	public function updateFlow()
	{
		if (particles.length >= maxParticles) 
		{ 
			return; 
		}
		addParticle(flowSrc.x,flowSrc.y);
		addParticle(flowSrc.x+12,flowSrc.y);
		addParticle(flowSrc.x-12,flowSrc.y);
	}

	public function update(dt: Float)
	{
		updateFlow();
		//updateImpulse(dt);
		applyGravity(dt);
		updateNeighbors();
		applyViscosity(dt);
		moveParticles(dt);
		doubleDensityRelaxation(dt);
		computeNextVelocity(dt);
	}

	public function updateImpulse(dt: Float)
	{
		for (p in particles)
		{
			if (p.pos.y+p.vel.y*dt>700 || p.pos.y+p.vel.y*dt<20)
			{
				p.vel.y *= -0.1;
			}

			if (p.pos.x+p.vel.x*dt>1260 || p.pos.x+p.vel.x*dt<20)
			{
				p.vel.x *= -0.1;
			}
		}
	}

	function doubleDensityRelaxation(dt: Float)
	{
		var p = 0.0;
		var pNear = 0.0;
		for (i in particles)
		{
			p = 0;
			pNear = 0.0;
			for (j in i.neighbors)
			{
				var q = (i.pos-j.pos).length / Drop.neighborDistance;
				if (q < 1)
				{
					p = p + (1-q)*(1-q);
					pNear = pNear + (1-q)*(1-q)*(1-q);
				}
			}

			var pressure = stiffnessK*(p-p0);
			var pressureNear = stiffnessK;

			var dx = Vector2.zero;
			for (j in i.neighbors)
			{
				var q = (i.pos-j.pos).length / Drop.neighborDistance;
				if (q < 1)
				{
					var tmpVec = i.pos.subtract(j.pos).normal;
					var displacement = dt*dt*(pressure*(1-q)+pressureNear*((1-q)*(1-q)))*tmpVec;
					j.pos = j.pos + 0.5*displacement;
					dx = dx - 0.5*displacement;
				}
			}
			i.pos += dx;
		}
	}

	function applyViscosity(dt: Float)
	{
		for (p in particles)
		{
			for (n in p.neighbors)
			{
				var q = (n.pos-p.pos).length/Drop.neighborDistance;
				if (q < 1)
				{
					var tmpVec = p.pos.subtract(n.pos).normal;
					var u = (p.vel - n.vel).dot(tmpVec);
					if (u > 0)
					{
						var I = dt*(1-q)*(sigmaViscousity*u + betaViscousity*u*u) * tmpVec;
						p.vel -= 0.5*I;
						n.vel += 0.5*I;
					}
				}
			}
		}
	}

	function applyGravity(dt: Float)
	{
		for (p in particles)
		{
			p.vel += dt*gravity;
		}
	}

	function moveParticles(dt: Float)
	{
		for (p in particles)
		{
			p.previousPosition = p.pos;
			p.pos += dt*p.vel;
			if (p.pos.y>=720)
			{
				delParticle(p);
			}
		}
	}

	function updateNeighbors()
	{
		sHash.clear();
		for (p in particles)
		{
			sHash.insert(p.pos, p);
			p.neighbors = sHash.queryPosition(p.pos);
		}
	}

	function computeNextVelocity(dt: Float)
	{
		for (p in particles)
		{
			p.vel = (1/dt)*(p.pos - p.previousPosition);
		}
	}

}