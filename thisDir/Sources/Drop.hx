package;

import hxmath.math.Vector2;

class Drop
{
	public var pos: Vector2 = new Vector2(0,0);
	public var vel: Vector2 = new Vector2(0,0);
	public var previousPosition = new Vector2(0,0);
	public var neighbors = new List<Drop>();
	var maxNeighbors = 5;
	public static var neighborDistance = 10;

	public function new(x: Float, y: Float){
		pos = new Vector2(x,y);
	}

	public function findAllNeighbors()
	{
		neighbors.clear();
		for (p in Khawater.solver.particles)
		{
			if (neighbors.length>maxNeighbors)
			{
				break;
			} else
			{
				if ((this.pos-p.pos).length <= neighborDistance)
				{
					neighbors.add(p);
				}
			}
		}
	}

}